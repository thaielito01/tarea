package Proyectos;

public class Ejercicio3 {

	public static void main(String[] args) {
		System.out.println("Tecla de Escape \t Significado");
		System.out.println("\n");
		System.out.println("\\n \t\t\t Nueva linea");
		System.out.println("\\t \t\t\t Hace una Tabulacion");
		System.out.println("\\\"\t\t\t Es para poner \"(comillas dobles) dentro del texto, por ejemplo \"Hola\" ");   
		System.out.println("\\\\ \t\t\t Es para escribir \\ dentro del texto, por ejemplo \\Hola\\");
		System.out.println("\\\' \t\t\t Es para escribir ' (comilla simple) dentro del texto, por ejemplo \'Hola\'");

	}

}